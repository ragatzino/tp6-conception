from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel
from config import get_config
import json

app = FastAPI()

    
data_path=get_config()['INIT_DATA']

def init_mots():
    with open(data_path) as json_file:  
        print(json_file)     
        print('poui')
        data = json.load(json_file)
        return data

mots=init_mots()
class Mot(BaseModel):
    id:int
    caracteres:str


@app.get("/")
def read_root():
    return mots

@app.get("/mots")
def get_mots():
    return mots

@app.get("/mot")
def add_mot(mot:Mot):
    return mots[0]

@app.post("/mot")
def add_mot(mot:Mot):
    mots.append(mot)
    return mots

@app.get("/items/{item_id}")
def get_items(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}
